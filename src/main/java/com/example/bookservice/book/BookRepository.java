package com.example.bookservice.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource

//@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book, Long> {

}