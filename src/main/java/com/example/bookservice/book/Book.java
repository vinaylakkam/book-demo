package com.example.bookservice.book;

import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class Book {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;

  @Size(min = 2)
  String title;

  String genre; // TODO: enum

  Integer publication;

  String author;

  public Book() {
  }

  public Book(Long id, String title, String genre, Integer publication, String author) {
    this.id = id;
    this.title = title;
    this.genre = genre;
    this.publication = publication;
    this.author = author;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getGenre() {
    return genre;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

  public Integer getPublication() {
    return publication;
  }

  public void setPublication(Integer publication) {
    this.publication = publication;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public String toString() {
    return "Book{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", genre='" + genre + '\'' +
            ", publication=" + publication +
            ", author='" + author + '\'' +
            '}';
  }
}
