package com.example.bookservice.book;

import com.example.bookservice.exception.BookException;
import com.example.bookservice.exception.BookNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

@Service
public class BookService {

  @Autowired
  BookRepository bookRepository;

  public List<Book> findAll() {
    return bookRepository.findAll();
  }

  public Book create(Book book) {
    return bookRepository.save(book);
  }

  public Book findById(Long bookId) {
    return bookRepository
            .findById(bookId)
            .orElseThrow(bookNotFound(bookId));

    /*final Optional<Book> optionalBook = bookRepository.findById(bookId);
    if (optionalBook.isPresent()) {
      return optionalBook.get();
    } else {
      throw new RuntimeException("Book not found");
    }*/
  }

  public void delete(Long bookId) {
    bookRepository.delete(this.findById(bookId));
  }


  public Book update(Long bookId, Book book) {
    if (bookId != book.getId()) {
      throw new BookException("Bad request: " + bookId);
    }

    return bookRepository
            .findById(bookId)
            .map(existingBook -> bookRepository.save(book))
            .orElseThrow(bookNotFound(bookId));

  }

  private Supplier<BookNotFoundException> bookNotFound(Long bookId) {
    return () -> new BookNotFoundException("Book not found: " + bookId);
  }
}
