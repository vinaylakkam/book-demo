package com.example.bookservice.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookResource {

  @Autowired
  BookService bookService;

  @GetMapping
  ResponseEntity<List<Book>> findAll() {
    return ResponseEntity
            .ok(bookService.findAll());
  }

  @PostMapping
  ResponseEntity<Book> create(@RequestBody @Valid Book book) {
    Book savedBook = bookService.create(book);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(savedBook.getId())
            .toUri();

    return ResponseEntity
            .created(location)
            .body(savedBook);
  }

  @PutMapping("{bookId}")
  ResponseEntity<Book> update(@PathVariable Long bookId,
                              @RequestBody Book book) {
    final URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentRequest().toUriString());
    return ResponseEntity.created(uri).body(bookService.update(bookId, book));
  }

  @GetMapping("{bookId}")
  ResponseEntity<Book> get(@PathVariable Long bookId) {
    return ResponseEntity.ok(bookService.findById(bookId));
  }

  @DeleteMapping("{bookId}")
  ResponseEntity<?> delete(@PathVariable Long bookId) {
    bookService.delete(bookId);
    return ResponseEntity.noContent().build();
  }
}
