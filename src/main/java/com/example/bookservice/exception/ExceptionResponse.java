package com.example.bookservice.exception;

import java.util.Date;

public class ExceptionResponse {
  Date date;
  String message;

  public ExceptionResponse(Date date, String message) {
    this.date = date;
    this.message = message;
  }
}
