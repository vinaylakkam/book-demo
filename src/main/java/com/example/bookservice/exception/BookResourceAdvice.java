package com.example.bookservice.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class BookResourceAdvice extends ResponseEntityExceptionHandler {

  @ExceptionHandler(BookNotFoundException.class)
  ResponseEntity handleNotFound(BookNotFoundException ex) {
    final ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage());
    return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(Exception.class)
  ResponseEntity handleException(Exception ex) {
    final ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage());
    return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
