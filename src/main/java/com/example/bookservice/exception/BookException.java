package com.example.bookservice.exception;

public class BookException extends RuntimeException {
  public BookException(String message) {
    super(message);
  }
}
